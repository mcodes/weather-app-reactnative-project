import React from 'react';
import { View } from 'react-native';
import Axios from 'axios'
import MyCities from './collections/myCities'


//Components Conditionally Rendered upon state view
import ManageCities from './components/ManageCities';
import Home from './components/Home';


class App extends React.Component {

  state = {
    date: '',
    currentWeather: { weather: [{ icon: '' }], main: { temp: '' }, wind: { deg: '' }, clouds: { all: '' } },
    hourlyWeather: [],
    weeklyWeather: [],
    view: 'home',
    myCities: [],
  
  }

  componentWillMount() {
    this.findCities()
    this.getDate()
    //getPosition calls getCurrent, Hourly and WeeklyWeather functions
    this.getPosition()
  }

  getTheme = () => {
    let d = new Date();
    let n = d.getHours();
    if (n > 20 || n < 6) {
      console.log('blue')
      // If time is after 8PM or before 6AM, apply night theme to ‘body’
      return {
        backgroundColor: 'blue'
      }

    } else if (n > 16 && n < 19) {
      console.log('orange')
      // If time is between 4PM – 7PM sunset theme to ‘body’
      return {
        backgroundColor: 'orange'
      }
    } else {
      console.log('yellow')

      // Else use ‘day’ theme
      return {
        backgroundColor: 'gold'
      }
    }
  }

  getWeeklyWeather = (lat, long) => {
    Axios.get(`http://api.openweathermap.org/data/2.5/forecast/daily?lat=${lat}&lon=${long}&units=metric&cnt=10&APPID=16909a97489bed275d13dbdea4e01f59`)
      .then(res => {
        this.setState({ weeklyWeather: res.data.list });
        // console.log(this.state.weeklyWeather)
      })
      .catch(function (e) {
        console.log(e.message)
      })
  }

  getHourlyWeather = (lat, long) => {
    Axios.get(`http://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${long}&units=metric&APPID=16909a97489bed275d13dbdea4e01f59&cnt=24`)
      .then(res => {
        this.setState({ hourlyWeather: res.data.list });
        // console.log(this.state.hourlyWeather.list)
      })
      .catch(function (e) {
        console.log(e.message)
      })
  }

  getCurrentWeather = (lat, long) => {
    // console.log('hello')
    Axios.get(`http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${long}&units=metric&APPID=16909a97489bed275d13dbdea4e01f59`)
      .then(res => {
        this.setState({ currentWeather: res.data }, () => console.log(this.state.currentWeather.main.temp));
      })
      .catch(function (e) {
        console.log(e.message);
      })
  }

  getPosition = () => {
    // navigator.geolocation.getCurrentPosition(position => { console.log(position) });
    // Position {coords: Coordinates, timestamp: 1557412461299}
    // accuracy: 40 altitude: null altitudeAccuracy: null heading: null latitude: 41.3957075 longitude: 2.1480845 speed: null 

    navigator.geolocation.getCurrentPosition(position => {
      this.getCurrentWeather(position.coords.latitude, position.coords.longitude)
      this.getHourlyWeather(position.coords.latitude, position.coords.longitude)
      this.getWeeklyWeather(position.coords.latitude, position.coords.longitude)
    })
  }

  getDate = () => {
    let date = new Date().toString().split(' ');
    date.splice(3)
    date = date.join(' ')

    this.setState({ date })
  }

  goHome = () => {
    this.getPosition()
  }

  goManageCities = (view) => {
    this.setState({ view })
  }




  // --------------------------------------------------------------------DB

  createCity = () => {
    //saving to local storage
    let { city } = this.state
    //we take from state and insert to db
    MyCities.insert({ city }, (err) => {
      err ? Alert.alert('error', err.message)
        : this.findCities()
    })
  }

  //everytime we modify cities we need to update state by calling function findTodos
  findCities = () => {
    MyCities.find({}, (err, myCities) => {
      err ? Alert.alert('error', err.message) : this.setState({ myCities })
    })
  }

  removeTodo = (_id) => {
    MyCities.remove({ _id }, (err) => {
      err ? Alert.alert('error', err.message)
        : this.findCities()
    })
  }





  render() {

    return (

      <View>

        {
          this.state.view === 'home' ?

            <View>
              <Home
                getCurrentWeather={this.getCurrentWeather}
                getHourlyWeather={this.getHourlyWeather}
                getWeeklyWeather={this.getWeeklyWeather}
                goManageCities={this.goManageCities}
                goHome={this.goHome}
                getDate={this.getDate}
                getPosition={this.getPosition}
                getTheme={this.getTheme}
                date={this.state.date}
                currentWeather={this.state.currentWeather}
                hourlyWeather={this.state.hourlyWeather}
                weeklyWeather={this.state.weeklyWeather}
              />
            </View>
            :
            <View>
              <ManageCities
                getCurrentWeather={this.getCurrentWeather}
                getHourlyWeather={this.getHourlyWeather}
                getWeeklyWeather={this.getWeeklyWeather}
                goManageCities={this.goManageCities}
                goHome={this.goHome}
              />
            </View>
        }

      </View>
    );
  }
}

export default App