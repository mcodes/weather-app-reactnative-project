import React from 'react'
import { StyleSheet, Text, View, Image, ScrollView } from 'react-native';
import { Icons } from './icons'
// import styles from './style.js'


class WeeklyDay extends React.Component {
  state = {
    day: ''
  }


  componentWillMount() {
    this.getDay(this.props.dt)
  }


  getDay = (dt) => {
    let day = new Date(dt * 1000).toString().split(' ');
    day.splice(1)

    this.setState({
      day
    })
  }





  render() {



    return (
      <View style={{ flexDirection: 'row', justifyContent: 'center', paddingVertical: 4 }}>

        <View style={[{ flexDirection: 'row-reverse', width: 100 }]}>

          <View>
            <Image
              source={Icons[this.props.weather[0].icon]}
              style={[{ width: 40, height: 40, backgroundColor: 'pink' }]}
            />
          </View>

          <View style={[styles.weeklyDayRainBar, { width: this.props.clouds / 2 }]}></View>
          <View style={[styles.weeklyDayRainBarTxt]}>

            <Text style={[styles.weeklyDayRainBarTxt]}>
              {this.props.clouds}%
            </Text>
          </View>

        </View>

        <View style={[ styles.border, { width: 56, justifyContent: 'center' }]}>
          <Text style={{ fontSize: 16, textAlign: 'center' }}>
            {this.state.day}
          </Text>
        </View>

        <View style={[{ flexDirection: 'row', width: 100 }]}>

          <View style={styles.weeklyDayTempMinBar}>
              <Text style={{ fontSize: 10 }}>
                {Math.floor(this.props.temp.min)} º
              </Text>
          </View>
          <View style={[styles.weeklyDayTempMaxBar, { width: this.props.temp.max * 2 }]}>
              <Text style={{ fontSize: 10 }}>
                {Math.floor(this.props.temp.max)} º
              </Text>
          </View>

        </View>

      </View>

    )
  }
}

export default WeeklyDay




const styles = StyleSheet.create({

  weeklyDayRainBar: {
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 4,
    borderBottomLeftRadius: 4,
    backgroundColor: 'pink',
    flexDirection: 'row'
  },
  weeklyDayRainBarTxt: {
    paddingRight: 4,
    justifyContent: 'center',
    fontSize: 10,
  },

  weeklyDayTempMaxBar: {
    alignItems: 'center',
    justifyContent: 'center',
    borderTopRightRadius: 4,
    borderBottomRightRadius: 4,
    backgroundColor: 'purple',
    // flexDirection: 'row'
  },

  weeklyDayTempMinBar: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'red',
    width: 40
  },

  border: {
    borderColor: 'gray',
    borderWidth: 0.8,
  },

});