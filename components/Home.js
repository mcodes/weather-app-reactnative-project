import React from 'react'
import { View, Text, ScrollView, StyleSheet, TouchableOpacity, Image } from 'react-native'
import { Icons } from './icons'
import TodayHour from './TodayHour/TodayHour'
import WeeklyDay from './WeeklyDay'

class Home extends React.Component {

  render() {

    return (

      <ScrollView stickyHeaderIndices={[1]} style={[styles.container, this.props.getTheme()]}>


        {/* *****************************   NAVBAR   ***************************** */}
        <View style={[styles.navBar, { flexDirection: 'row', justifyContent: "space-between" }]}>
          <TouchableOpacity onPress={this.props.goHome} style={styles.searchBarCurrLoc}>
            <Image source={Icons.here} style={{ width: 24, height: 24 }} />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.props.goManageCities('manageCities')} style={[styles.searchBarManageCities]}>
            <Image source={Icons.world} style={{ width: 34, height: 34 }} />
          </TouchableOpacity>
        </View>


        {/* *****************************   HEADER   ***************************** */}
        <View style={[styles.border, styles.header]}>
          <Text style={[styles.border, { fontSize: 32 }]}>{this.props.currentWeather.name}</Text>
          <Text style={{ alignSelf: 'flex-end' }}>{this.props.date}</Text>
        </View>


        {/* *****************************   CURRENT   ***************************** */}
        <View style={[styles.current]}>


          <View style={styles.currentIcon}>
            <Image
              source={Icons[this.props.currentWeather.weather[0].icon]}
              // style={[styles.border, styles.stretch,{backgroundColor: 'red'}]}
              style={[{ width: 200, height: 200 }]}
            />
          </View>

          <View style={styles.currentInfo}>
            <Text style={{ fontSize: 48 }}>{Math.round(this.props.currentWeather.main.temp)}ºC</Text>
            <Text style={{ textTransform: 'capitalize' }}>{this.props.currentWeather.weather[0].description}</Text>
            <Text>{Math.round(this.props.currentWeather.main.temp_min)} / {Math.round(this.props.currentWeather.main.temp_max)} ºC</Text>
          </View>


        </View>

        {/* CURRENT DETAILS */}
        <View>
          <Text style={{ fontSize: 18 }}>Current Details</Text>
          <View style={[styles.currentDetails]}>
            <View>
              <Text style={{ fontSize: 14 }}>Humidity{Math.round(this.props.currentWeather.main.humidity)}%</Text>
              <Text style={{ fontSize: 14 }}>Pressure{Math.round(this.props.currentWeather.main.pressure)}hpa</Text>
            </View>
            <View>
              <Text style={{ fontSize: 14 }}>Wind{Math.round(this.props.currentWeather.wind.deg)}º</Text>
              <Text style={{ fontSize: 14 }}>{Math.round(this.props.currentWeather.wind.speed)}meter/sec</Text>
            </View>
            <View>
              <Text style={{ fontSize: 14 }}>{this.props.currentWeather.clouds.all}% clouds</Text>
            </View>
            {/* <Text>{Math.round(this.state.currentWeather.main.temp_min)} / {Math.round(this.state.currentWeather.main.temp_max)} ºC</Text> */}
          </View>
        </View>


        {/* *****************************   TODAY   ***************************** */}

        <View style={[styles.today]}>
          <Text style={{ fontSize: 24 }}>Today</Text>
          {/* TODAY-HOUR INFO */}
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{ flexDirection: 'row', marginVertical: 14 }} >


            {/* HOURINFO */}

            {
              this.props.hourlyWeather.map((hour, i) => {

                return (
                  <TodayHour key={i} {...hour}
                  // hourlyWeather={this.state.hourlyWeather}
                  />
                );
              })
            }

          </ScrollView>{/* END OF TODAY INFO */}

        </View>


        {/* *****************************   THIS WEEK   ***************************** */}

        <View style={[styles.week]}>
          <Text style={{ fontSize: 24 }}>This Week</Text>

          <View style={styles.weeklyDayContainer}>

            {
              this.props.weeklyWeather.map((day, i) => {

                return (

                  <WeeklyDay key={i} {...day} />
                );
              })
            }

          </View>

        </View>
      </ScrollView>



    )
  }
}

const styles = StyleSheet.create({

  container: {
    // paddingHorizontal: 24,
    // height: '200%',
    flexGrow: 1,
    backgroundColor: 'red',
    paddingTop: 24,

    // alignItems: 'center',
    // justifyContent: 'center',
  },

  header: {
    // height: '7%',
    // width: '100%',
    // backgroundColor: 'white',
    flexDirection: 'row',
    borderBottomColor: 'gold',
    borderBottomWidth: 1,
  },

  current: {
    // height:'100%',
    // width: '100%',
    flexDirection: 'row'
  },

  currentIcon: {
    // height:'100%',
    width: '60%',
    backgroundColor: 'deeppink',
  },

  currentInfo: {
    // height:'100%',
    width: '50%',
    backgroundColor: 'blue',
    justifyContent: 'flex-end'
  },

  currentDetails: {
    // height:'100%',
    // width: '100%',
    justifyContent: 'space-around',
    flexDirection: 'row',
    paddingVertical: 14,
  },

  today: {
    // minHeight: 300,
    // width: '100%',
    backgroundColor: 'green'
  },

  week: {
    minHeight: 300,
    // width: '100%',
    backgroundColor: 'orange'
  },

  miniIcons: {
    width: 40,
    height: 40
  },

  weeklyDayContainer: {
    alignItems: 'center',
    justifyContent: 'center'
  },


  // -------------------navbar

  navBar: {
    backgroundColor: 'purple',
    padding: 8,
  },

  searchBarCurrLoc: {
    borderRadius: 100,
    backgroundColor: 'gold',
    width: 38,
    height: 38,
    alignItems: 'center',
    justifyContent: 'center'
  },

  searchBarManageCities: {
    borderRadius: 100,
    backgroundColor: 'white',
    width: 38,
    height: 38,
    alignItems: 'center',
    justifyContent: 'center'

  },







  stretch: {
    width: 200,
    height: 200
  },
  border: {
    borderColor: 'gray',
    borderWidth: 0.8,
  },
  row: {
    flexDirection: 'row',
  },
  flex5: {
    flex: .5,
  },
  flexEnd: {
    alignItems: 'flex-end',
  },
  bgRed: {
    backgroundColor: 'red',
  },
  bgBlue: {
    backgroundColor: 'blue',
  },
});

export default Home