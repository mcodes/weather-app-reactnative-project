import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native'
import Map from './Map'
import { Icons } from './icons'


class ManageCities extends React.Component {

 

    render() {



        return (


            <View style={{ padding: 30, height: "100%", backgroundColor: 'blue' }}>
                <Map style={{ flex: 1, backgroundColor: 'red' }}
                    getCurrentWeather={this.props.getCurrentWeather}
                    getHourlyWeather={this.props.getHourlyWeather}
                    getWeeklyWeather={this.props.getWeeklyWeather}
                    goManageCities={this.props.goManageCities}
                />
                <TouchableOpacity onPress={() => this.props.goManageCities('home')} style={[styles.searchBarManageCities]}>
                    <Image source={Icons.here} style={{ width: 34, height: 34 }} />
                </TouchableOpacity>
                {/* <TouchableOpacity onPress={this.props.goHome} style={styles.searchBarCurrLoc}>
                    <Image source={Icons.world} style={{ width: 24, height: 24 }} />
                </TouchableOpacity> */}
            </View>

        )
    }
}



const styles = StyleSheet.create({

    searchBarManageCities: {
        borderRadius: 100,
        backgroundColor: 'white',
        width: 38,
        height: 38,
        alignItems: 'center',
        justifyContent: 'center'

    },
    searchBarCurrLoc: {
        borderRadius: 100,
        backgroundColor: 'gold',
        width: 38,
        height: 38,
        alignItems: 'center',
        justifyContent: 'center'
      },


})



export default ManageCities 