import React from 'react'
import { StyleSheet, Text, View, Image, ScrollView } from 'react-native';
import { Icons } from '../icons'
// import styles from './style.js'


class TodayHour extends React.Component {
  state = {}

  render() {
    // console.log(this.props)


    return (
      <View style={{ flexDirection: 'row', alignItems: "flex-end",  }} >
        {/* HOURINFO */}
        <View style={[styles.todayHourInfo, { width: 40 }]}>
          
          <View style={[styles.todayHourInfoTempBar, { height: this.props.main.temp*2 } ]} >
            <View>
            <Text style={[ {fontSize: 12 }  ]}>{Math.round(this.props.main.temp)}ºC</Text>
            </View>
            
          </View>
          <View>
            <Text style={{ fontSize: 12, textAlign: 'center' }}>
              {(this.props.dt_txt).split(" ")[1].substring(0,5)}
            </Text>
          </View>

          <View>
            <Image
              source={Icons[this.props.weather[0].icon]}
              style={[ styles.todayHourInfoIcons]}
            />
          </View>
        </View>

      </View>

    )
  }
}

export default TodayHour

const styles = StyleSheet.create({

  todayHourInfo: {
    marginLeft:4,
  },

  todayHourInfoIcons: {
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },

  todayHourInfoTempBar: {
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: 4, 
    borderTopRightRadius: 4,  
    backgroundColor: 'blue'
  },



  stretch: {
    width: 200,
    height: 200
  },
  border: {
    borderColor: 'gray',
    borderWidth: 0.8,
  },
  row: {
    flexDirection: 'row',
  },
  flex5: {
    flex: .5,
  },
  flexEnd: {
    alignItems: 'flex-end',
  },
  bgRed: {
    backgroundColor: 'red',
  },
  bgBlue: {
    backgroundColor: 'blue',
  },
});

