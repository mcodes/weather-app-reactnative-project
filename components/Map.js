import React from "react";
import { View } from "react-native";
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete";
/*
The component is child component for location search 
the selected location can be stored in state variable
*/
class Map extends React.Component {
    constructor(props) {
        super(props);
        this.state = { }
    }

    componentWillUnmount() {
        this.props.getCurrentWeather(this.state.lat, this.state.lng)
        this.props.getHourlyWeather(this.state.lat, this.state.lng)
        this.props.getWeeklyWeather(this.state.lat, this.state.lng)
    }


    render() {
        return (
            <View style={{ flex: 1 }}>
                <GooglePlacesAutocomplete

                    placeholder="Enter city..."
                    minLength={2} // minimum length of text to search
                    autoFocus={false}
                    returnKeyType={"search"}
                    listViewDisplayed="false"
                    fetchDetails={true}
                    renderDescription={row =>
                        row.description || row.formatted_address || row.name
                    }
                    onPress={(data, details = null) => {
                        this.setState({lat: details.geometry.location.lat, lng: details.geometry.location.lng},()=>this.props.goManageCities('home') ) 
                        

                    }}
                    getDefaultValue={() => {
                        return ""; // text input default value
                    }}
                    query={{
                        key: 'AIzaSyAM0XzUi7DdcoKGjCt7rSDjhhPimA2kAFU',
                        language: "en", // language of the results
                        types: "(cities)" // default: 'geocode'
                    }}
                    styles={{
                        description: {
                            fontWeight: "bold"
                        },
                        textInputContainer: {
                            backgroundColor: 'none'
                        },
                        textInput: {
                            width: '60%',
                        },
                        predefinedPlacesDescription: {
                            color: "#1faadb"
                        }
                    }}
                    enablePoweredByContainer={false}
                    nearbyPlacesAPI="GoogleReverseGeocoding"
                    GooglePlacesSearchQuery={{
                        rankby: "distance",
                        types: "food"
                    }}
                    filterReverseGeocodingByTypes={[
                        "locality",
                        "administrative_area_level_3"
                    ]}
                    debounce={200}
                />
            </View>
        );
    }
}

export default Map